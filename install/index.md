## Raspberry Pi
Para instalar ooniprobe en dispositivos Raspberry pi vea nuestra [guía de instalación de lepidopter](https://ooni.torproject.org/install/lepidopter/)


## OS X and Linux
Para instalar ooniprobe en sistemas basados en unix, lea nuestra guía de instalación
