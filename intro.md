El Observatorio Abierto de Inteferencia de Red (OONI, por sus siglas en inglés) es un proyecto de software libre que busca promover esfuerzos descentralizados para aumentar la transparencia sobre la censura en Internet alrededor del mundo. Con este fin, OONI desarrolla un software con una gran variedad de pruebas, llamado ooniprobe, estas pruebas están diseñadas para medir:

  * Bloqueo de sitios web;
  * Bloqueo de aplicaciones de mensajería instantánea (WhatsApp, Facebook Messenger, Telegram);
  * Bloqueo de herramientas para evadir la censura (Tor, Psiphon, Lantern, OpenVPN);
  * Detección de sistemas ("middleboxes") que pueden ser responsables de censura, vigilancia, y/o manipulación de tráfico;
  * Velocidad y desempeño de redes.

Cualquier persona puede ejecutar las pruebas de ooniprobe para recolectar información de las redes en las que está conectada. Esta información puede servir potencialmente como *evidencia de censura en Internet*, dado que permite saber qué está siendo bloqueado, cómo, y quién lo está haciendo.

Para aumentar la transparencia sobre la censura en Internet, toda la información recolectada se publicada en:

https://explorer.ooni.torproject.org/world/

Desde 2012, decenas de miles de personas han ejecutado ooniprobe en más de 190 países, dando luz sobre el bloqueo de sitios web de medios de comunicación, aplicaciones de mensajería instantánea, y páginas de derechos humanos (entre muchos otros tipos de sitios web y servicios) durante eventos políticos como elecciones y protestas. Puedes conocer más sobre estos casos en https://ooni.torproject.org/post/

Al ejecutar ooniprobe puedes ayudar a documentar como funciona Internet (o como no funciona), y puedes ayudar a proporcionar al público los datos necesarios sobre los (potencialmente secretos) controles de información.

Puedes instalar ooniprobe en múltiples plataformas:

https://ooni.torproject.org/install/

Conoce más sobre el proyecto aquí: htttps://ooni.torproject.org/

Puedes suscribirte a la lista de correo de OONI aquí:
https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-talk

Happy testing!
