##Comunidad

Únasenos en [#ooni en el IRC de OFTC](ircs://irc.oftc.net:6697/#ooni), en [Slack](https://slack.openobservatory.org/) o en las listas de distribución [ooni-talk](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-talk) y [ooni-dev](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-dev) para discutir sobre el proyecto, qué ha aprendido de las mediciones y en qué forma le podemos ayudar

Si está contribuyendo con mediciones de forma regular asegúrese de suscribirte también a la lista de [ooni-operators](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-operators) para obtener actualizaciones importantes.


## Enviar URLs para probar

¡Ayúdenos a detectar censura enviándonos direcciones URL para probar! Vea más [aquí](https://ooni.torproject.org/get-involved/contribute-test-lists)
Help us detect censorship by submitting URLs for testing! Learn more here.

## Alianzas

¿Interesad@ en explorar la censura en internet? Vea más sobre nuestro [programa de alianzas](https://ooni.torproject.org/get-involved/partnership-program/)

## Código y documentación para desarrolladores

Nuestro código fuente está disponible en [Github](https://github.com/TheTorProject/ooni-probe) y sincronizado con el [servidor git del Proyecto Tor](https://gitweb.torproject.org/ooni-probe.git). Si tiene código que quiera contribuir puede [hacer un *pull request*](https://github.com/TheTorProject/ooni-probe/compare) o [abrir un *issue*](https://github.com/TheTorProject/ooni-probe/issues/new)

Si está interesado en modificar OONI o escribir sus propias pruebas, puede ver la [documentación para desarrolladores](https://ooni.torproject.org/docs/).
