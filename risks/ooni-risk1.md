## Riesgos de ejecutar ooniprobe

**PRECAUCIÓN:** Ejecutar ooniprobe tiene riesgos asociados.

En algunos países, usar herramientas de medición activa de red como ooniprobe puede ser ilegal, e incluso considerado espionaje. Las sanciones potenciales incluyen prisión, ataques físicos, grandes multas, amenazas, inclusión en listas de monitoreo del estado y elección como blanco para ser vigilado.

Algunas pruebas de ooniprobe pueden conectarse a sitios web considerados ilegales en su país, y las mismas pruebas pueden violar leyes de mal uso de equipos computacionales. OONI publica los datos de todas las pruebas recolectadas, los cuales pueden tener información que lo identifique personalmente. De acuerdo a esto, terceras partes con habilidades sofisticadas, incluyendo proveedores de servicio de internet (ISP por sus siglas en Inglés) y lo administradores de los sitios web, pueden ser capaces de detectar que está usando ooniprobe y reportarlo a las autoridades. Recomendamos que consulte a un abogado licenciado para ejercer en su jurisdicción antes de descargar y utilizar ooniprobe.


Los usuarios que ejecutan ooniprobe lo hacen bajo su propio riesgo. Al instalar ooniprobe, los usuarios están de acuerdo con cumplir con la [licencia](https://github.com/TheTorProject/ooni-%20probe/blob/master/LICENSE) y [Política de Datos](https://ooni.torproject.org/about/data-policy) de OONI. Ni el [Proyecto OONI](https://ooni.torproject.org/) ni su organización matriz, [El Proyecto Tor](https://www.torproject.org/), se hacen responsables, de forma conjunta o solidaria, ante la ley o en equidad, ante usuarios de ooniprobe y terceros por cualquier daño resultante del uso de ooniprobe bajo cualquier agravio, contrato u otras causas de acción.

Por favor, lea la [documentación](https://ooni.torproject.org/about/risks/) relevante para aprender más sobre los riesgos potenciales asociados a la ejecución de ooniprobe.

-------------------

Users run ooniprobe at their own risk. By installing ooniprobe, users agree to comply with OONI’s software license and Data Policy. Neither the OONI project nor its parent organization, The Tor Project, will be held liable, jointly or severally, at law or at equity, to ooniprobe users and other third parties for any risks or damages resulting from the use of ooniprobe under any tort, conract, or other causes of action.
